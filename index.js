const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {

		
		if(request.url == "/booking" && request.method == "GET"){
			response.writeHead(200, {"Content-Type": "text/plain"})
			response.end("Welcome to booking system");
		}

		if(request.url == "/profile" && request.method == "GET"){
			response.writeHead(200, {"Content-Type": "text/plain"})
			response.end("Welcome to your profile");
		}

		if(request.url == "/course" && request.method == "GET"){
			response.writeHead(200, {"Content-Type": "text/plain"})
			response.end("Here's our courses available");
		}

		if(request.url == "/addCourse" && request.method == "POST"){
			response.writeHead(200, {"Content-Type": "text/plain"})
			response.end("Add course to our resources");
		}

		if(request.url == "/updateCourse" && request.method == "PUT"){
			response.writeHead(200, {"Content-Type": "text/plain"})
			response.end("Update a course to our resourses");
		}

		if(request.url == "/archiveCourse" && request.method == "DELETE"){
			response.writeHead(200, {"Content-Type": "text/plain"})
			response.end("Archive courses to our resources");
		}


});

server.listen(port);
console.log(`Server is Good at localhost: ${port}`);